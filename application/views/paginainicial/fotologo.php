<div>
    <div class="container">
    <br><br><br>
    <!--Inicio Grade Linha-->
    <div class="row text-center wow fadeIn">
        <!--Inico Grade coluna-->
        <div class="col-lg-6 col-md-12 mb-4">
            <div class="">
                <div class="view overlay" align="center">
                    <img src="assets/mdb/img/catrina.png" alt="grafico" width=70%>
                </div>
            </div>
        </div>
        <!--Fim Grade coluna-->
        <!--Inicio Grade Coluna-->
        <div class="col-lg-6 col-md-12 mb-4">
            <div class="">
                <div class="view overlay" align="center">
                    <img src="assets/mdb/img/netostudio.png" alt="grafico" width=100% height="300">
                </div>
                <div class="card-body">
                    <p class="card-text"><h5 style="color:white;">
                        Nossos tatuadores usam tecnologia 100% segura para criar obras de arte impressionantes. Eles usam equipamentos certificados, o que garante os melhores resultados possíveis.</h5>
                    </p>
                    <button type="button" class="efeitopersonalizado btn btn-elegant">Saiba Mais...</button>
                </div>
            </div>
        </div>
        <!--Fim Grade coluna-->
    </div>
    <!--Fim Grade Linha-->
</div>