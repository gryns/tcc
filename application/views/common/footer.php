    <footer class="page-footer text-center font-small mt-5 wow fadeIn elegant-color-dark">
    <hr class="my-4">
    <div class="pb-4">
        <a href="#" target="">
            <i class="fa fa-facebook mr-3"></i>
        </a>
        <a href="#" target="">
            <i class="fa fa-twitter mr-3"></i>
        </a>
        <a href="#" target="">
            <i class="fa fa-youtube mr-3"></i>
        </a>
    </div>
    <div class="footer-copyright py-3">
        © 2018 Copyright:
        <a href="#" target=""> Gryns </a>
    </div>
    </footer>
    <script type="text/javascript" src="assets/mdb/js/jquery-3.3.1.min.js"></script>
    <script type="text/javascript" src="assets/mdb/js/popper.min.js"></script>
    <script type="text/javascript" src="assets/mdb/js/bootstrap.min.js"></script>
    <script type="text/javascript" src="assets/mdb/js/mdb.min.js"></script>
    <script type="text/javascript">
        new WOW().init();
    </script>
    <!-- Meu Script Personalizado -->
    <script type="text/javascript">
        
        //Fecha SideNav Clicando no Body
        $("#body").bind("click",function(){
            if($('#mySidenav').is(":visible")){
                $("#botaoabre").click();  
            }
        });

        $("#botaoabre").bind("click",function() {
            if($('#mySidenav').is(":visible")){
                document.getElementById("menu").className = "menundesfocado";  
            }
            else{
                document.getElementById("menu").className = "menudesfocado";
            };
        });

        //Abre e fecha Sidenav clicando no botão hamburgue
        $(document).ready(function() {		
            $("#mySidenav").hide();
            $("#botaoabre").bind("click",function(){
                $("#mySidenav").slideToggle(300);
                return false;
            });
        });
        //Animação do botão hamburguer
        $(document).ready(function () {
            $('.first-button').on('click', function () {
                $('.animated-icon1').toggleClass('open');
            });
            $('.second-button').on('click', function () {
                $('.animated-icon2').toggleClass('open');
            });
            $('.third-button').on('click', function () {
                $('.animated-icon3').toggleClass('open');
            });
        });
    </script>
</main>
</body>
</html>
