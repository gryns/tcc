<!DOCTYPE html>
<!--
@     @ @     @ @@@@@@  @@   @  @@@@
@@   @@ @     @ @       @ @  @ @
  @@@   @     @ @@@@    @  @ @  @@@@ 
   @    @@   @@ @       @   @@      @ 
   @      @@@   @@@@@@@ @    @ @@@@@
                           
                         @
@@@@   @@@@@@  @@   @ @@@@@@  @@@@@@
@   @  @       @ @  @ @       @
@@@@@  @@@@    @  @ @ @@@@    @@@@
@    @ @       @   @@ @       @
@@@@@  @@@@@@@ @    @ @@@@@@@ @@@@@@@
-->
<html>
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta http-equiv="x-ua-compatible" content="ie=edge">
  <title>Netto Tattoo</title>
  <link rel='shortcut icon' href="assets/mdb/img/catrina.png" />
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
  <link href="assets/mdb/css/bootstrap.min.css" rel="stylesheet">
  <link href="assets/mdb/css/mdb.min.css" rel="stylesheet">
  <link href="assets/mdb/css/style.min.css" rel="stylesheet">
  <style>

    .menudesfocado{
      filter: blur(2px);
      margin-top: 25px;
    }
    .menundesfocado{
      filter: blur(0);
      margin-top: 25px;
    }
    /* Navbar Personalizada
      Icon 1 */

    .animated-icon1, .animated-icon2, .animated-icon3 {
      width: 30px;
      height: 20px;
      position: relative;
      margin: 0px;
      -webkit-transform: rotate(0deg);
      -moz-transform: rotate(0deg);
      -o-transform: rotate(0deg);
      transform: rotate(0deg);
      -webkit-transition: .5s ease-in-out;
      -moz-transition: .5s ease-in-out;
      -o-transition: .5s ease-in-out;
      transition: .5s ease-in-out;
      cursor: pointer;
      }
      
      .animated-icon1 span, .animated-icon2 span, .animated-icon3 span {
      display: block;
      position: absolute;
      height: 3px;
      width: 100%;
      border-radius: 9px;
      opacity: 1;
      left: 0;
      -webkit-transform: rotate(0deg);
      -moz-transform: rotate(0deg);
      -o-transform: rotate(0deg);
      transform: rotate(0deg);
      -webkit-transition: .25s ease-in-out;
      -moz-transition: .25s ease-in-out;
      -o-transition: .25s ease-in-out;
      transition: .25s ease-in-out;
      }
      
      .animated-icon1 span {
      background: #ff0000;
      }
      
      .animated-icon2 span {
      background: #ff0000;
      }
      
      .animated-icon3 span {
      background: #ff0000;
      }
      
      .animated-icon1 span:nth-child(1) {
      top: 0px;
      }
      
      .animated-icon1 span:nth-child(2) {
      top: 10px;
      }
      
      .animated-icon1 span:nth-child(3) {
      top: 20px;
      }
      
      .animated-icon1.open span:nth-child(1) {
      top: 11px;
      -webkit-transform: rotate(135deg);
      -moz-transform: rotate(135deg);
      -o-transform: rotate(135deg);
      transform: rotate(135deg);
      }
      
      .animated-icon1.open span:nth-child(2) {
      opacity: 0;
      left: -60px;
      }
      
      .animated-icon1.open span:nth-child(3) {
      top: 11px;
      -webkit-transform: rotate(-135deg);
      -moz-transform: rotate(-135deg);
      -o-transform: rotate(-135deg);
      transform: rotate(-135deg);
      }
      
      /* Icon 3*/
      
      .animated-icon2 span:nth-child(1) {
      top: 0px;
      }
      
      .animated-icon2 span:nth-child(2), .animated-icon2 span:nth-child(3) {
      top: 10px;
      }
      
      .animated-icon2 span:nth-child(4) {
      top: 20px;
      }
      
      .animated-icon2.open span:nth-child(1) {
      top: 11px;
      width: 0%;
      left: 50%;
      }
      
      .animated-icon2.open span:nth-child(2) {
      -webkit-transform: rotate(45deg);
      -moz-transform: rotate(45deg);
      -o-transform: rotate(45deg);
      transform: rotate(45deg);
      }
      
      .animated-icon2.open span:nth-child(3) {
      -webkit-transform: rotate(-45deg);
      -moz-transform: rotate(-45deg);
      -o-transform: rotate(-45deg);
      transform: rotate(-45deg);
      }
      
      .animated-icon2.open span:nth-child(4) {
      top: 11px;
      width: 0%;
      left: 50%;
      }
      
      /* Icon 4 */
      
      .animated-icon3 span:nth-child(1) {
      top: 0px;
      -webkit-transform-origin: left center;
      -moz-transform-origin: left center;
      -o-transform-origin: left center;
      transform-origin: left center;
      }
      
      .animated-icon3 span:nth-child(2) {
      top: 10px;
      -webkit-transform-origin: left center;
      -moz-transform-origin: left center;
      -o-transform-origin: left center;
      transform-origin: left center;
      }
      
      .animated-icon3 span:nth-child(3) {
      top: 20px;
      -webkit-transform-origin: left center;
      -moz-transform-origin: left center;
      -o-transform-origin: left center;
      transform-origin: left center;
      }
      
      .animated-icon3.open span:nth-child(1) {
      -webkit-transform: rotate(45deg);
      -moz-transform: rotate(45deg);
      -o-transform: rotate(45deg);
      transform: rotate(45deg);
      top: 0px;
      left: 8px;
      }
      
      .animated-icon3.open span:nth-child(2) {
      width: 0%;
      opacity: 0;
      }
      
      .animated-icon3.open span:nth-child(3) {
      -webkit-transform: rotate(-45deg);
      -moz-transform: rotate(-45deg);
      -o-transform: rotate(-45deg);
      transform: rotate(-45deg);
      top: 21px;
      left: 8px;
      }

    /*Fim da Navbar Personalizada */

    /* Navbar Teste */
    .sidenav{
      height: 100%;
      width: 250px;
      position: fixed;
      z-index: 1;
      top: 0;
      right: 0;
      overflow-x: hidden;
      padding-top: 60px;
      background-color: #212121;
      display: none;
    }
    .sidenav a{
      padding: 8px 8px 8px 32px;
      text-decoration: none;
      font-size: 25px;
      display: block;
      color: #ff0000;
      transition: 0.3s;
    }
    .sidenav a:hover{
      color: #ff0000;
    }
    .sidenav .closebtn{
      position: absolute;
      top: 0;
      left: 5px;
      font-size: 35px;
    }
    @media screen and (max-height: 450px){
      .sidenav{padding-top: 15px;}
      .sidenav a{font-size: 18px;}
    }
    
  </style>
</head>
<body id="body">

